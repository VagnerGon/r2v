#include <opencv2//core//core.hpp>
#include <opencv2//highgui//highgui.hpp>

#include <ctime>

#include "R2V.h"
#include <iostream>

int main(int argc, char* argv[]){

	clock_t start,finish;

	start = clock();

	const char* mensagemEntrada = argc == 1 ? "img.jpg" : argv[1]; 

	IplImage *imagem = cvLoadImage(mensagemEntrada, CV_LOAD_IMAGE_GRAYSCALE);
	unsigned int* histograma = (unsigned int*)malloc(sizeof(unsigned int)*256);
	
	R2V((unsigned char*) imagem->imageData, histograma, imagem->imageSize);

	for(int i=0; i<256;i++) {
		std::cout << histograma[i] << " ";
	}

	finish = clock() - start;
	double interval = finish / (double)CLOCKS_PER_SEC; 
	//std::cout << "Resultado: " << +maior << std::endl;
	std::cout << "tempo : " << interval * 1000 << "ms" << std::endl;

	return 0;
}
