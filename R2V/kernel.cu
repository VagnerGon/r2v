#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <iostream>

#include <stdio.h>

//# define NBINS 64

#define UINT_BITS 32
#define LOG2_WARP_SIZE 5U
#define WARP_SIZE (1U << LOG2_WARP_SIZE)

#define HISTOGRAM256_BIN_COUNT 256
#define MERGE_THREADBLOCK_SIZE 256

//Threadblock size
#define HISTOGRAM256_THREADBLOCK_SIZE (WARP_COUNT * WARP_SIZE)

//Shared memory per threadblock
#define HISTOGRAM256_THREADBLOCK_MEMORY (WARP_COUNT * HISTOGRAM256_BIN_COUNT)

//Warps ==subhistograms per threadblock
#define WARP_COUNT 6

#define CHK_ERROR if (erro != cudaSuccess) goto Error;

__device__ void doStuff(unsigned int* s_warp_hist, unsigned int data, const unsigned tag) {
	
	atomicAdd(&s_warp_hist[data], 1); //s_warp_hist + data
	/*unsigned int count;
	do
    {
        count = s_warp_hist[data];
        count = tag | (count + 1);
        s_warp_hist[data] = count;
    }
    while (s_warp_hist[data] != count);*/

	//s_warp_hist[data]++;
}

__global__ void R2V_kernel(unsigned char* imagem, unsigned int* result, int bins, int tamanho) {

	//Per-warp subhistogram storage
    __shared__ unsigned int s_Hist[HISTOGRAM256_THREADBLOCK_MEMORY];
    unsigned int *s_WarpHist= s_Hist + (threadIdx.x >> LOG2_WARP_SIZE) * HISTOGRAM256_BIN_COUNT;

    //Clear shared memory storage for current threadblock before processing

    for (unsigned int i = 0; i < (HISTOGRAM256_THREADBLOCK_MEMORY / HISTOGRAM256_THREADBLOCK_SIZE); i++)
    {
        s_Hist[threadIdx.x + i * HISTOGRAM256_THREADBLOCK_SIZE] = 0;
    }

    //Cycle through the entire data set, update subhistograms for each warp
    const unsigned int tag = threadIdx.x << (UINT_BITS - LOG2_WARP_SIZE);

    __syncthreads();

	//Faz acesso nao coescalado para evitar conflitos de memoria compartilhada
	for (unsigned int pos = blockIdx.x * blockDim.x + threadIdx.x; pos < tamanho; pos += blockDim.x * gridDim.x) {
        unsigned int data = imagem[pos];
        doStuff(s_WarpHist, data, tag);
    }

    __syncthreads();

	//Faz uma merge semando as respctivas posicos de cada subhistograma dos warps para o histograma do bloco, gravado na memoria global.
	for (unsigned int bin = threadIdx.x; bin < HISTOGRAM256_BIN_COUNT; bin += HISTOGRAM256_THREADBLOCK_SIZE)
    {
        unsigned int sum = 0;

        for (unsigned int i = 0; i < WARP_COUNT; i++)
        {
            sum += s_Hist[bin + i * HISTOGRAM256_BIN_COUNT]; //& TAG_MASK
        }

        result[blockIdx.x * HISTOGRAM256_BIN_COUNT + bin] = sum;
    }
}

__global__ void mergeHistogram256Kernel(unsigned int *d_Histogram, unsigned int *d_PartialHistograms, unsigned int histogramCount) {
    
	unsigned int sum = 0;

    for (unsigned int i = threadIdx.x; i < histogramCount; i += MERGE_THREADBLOCK_SIZE)
    {
        sum += d_PartialHistograms[blockIdx.x + i * HISTOGRAM256_BIN_COUNT];
    }

    __shared__ unsigned int data[MERGE_THREADBLOCK_SIZE];
    data[threadIdx.x] = sum;

    for (unsigned int stride = MERGE_THREADBLOCK_SIZE / 2; stride > 0; stride >>= 1)
    {
        __syncthreads();

        if (threadIdx.x < stride)
        {
            data[threadIdx.x] += data[threadIdx.x + stride];
        }
    }

    if (threadIdx.x == 0)
    {
        d_Histogram[blockIdx.x] = data[0];
    }
}

static unsigned int* d_PartialHistograms;
//histogram256kernel() intermediate results buffer
static const unsigned int PARTIAL_HISTOGRAM256_COUNT = 240;

extern "C" void R2V(unsigned char* imagem, unsigned int* histogram, int tamanho) {
	
	unsigned char* d_image;
	unsigned int* d_Histogram;

	cudaError_t erro;	
			
	erro = cudaMalloc((void**)&d_image,sizeof(unsigned char)*tamanho); CHK_ERROR

	erro =cudaMalloc((void **)&d_PartialHistograms, PARTIAL_HISTOGRAM256_COUNT * HISTOGRAM256_BIN_COUNT * sizeof(unsigned int)); CHK_ERROR
	
	erro =cudaMalloc((void **)&d_Histogram, HISTOGRAM256_BIN_COUNT * sizeof(unsigned int)); CHK_ERROR
	
	erro = cudaMemcpy(d_image,imagem,tamanho*sizeof(unsigned char),cudaMemcpyHostToDevice);

	R2V_kernel<<<PARTIAL_HISTOGRAM256_COUNT, HISTOGRAM256_THREADBLOCK_SIZE>>>(d_image, (unsigned int*) d_PartialHistograms, HISTOGRAM256_BIN_COUNT, tamanho / sizeof(unsigned char));

	erro = cudaGetLastError(); CHK_ERROR
	erro = cudaDeviceSynchronize(); CHK_ERROR

	mergeHistogram256Kernel<<<HISTOGRAM256_BIN_COUNT, MERGE_THREADBLOCK_SIZE>>>(d_Histogram, d_PartialHistograms, PARTIAL_HISTOGRAM256_COUNT);

	erro = cudaGetLastError(); CHK_ERROR	
	erro = cudaDeviceSynchronize(); CHK_ERROR

	erro = cudaMemcpy(histogram,d_Histogram,HISTOGRAM256_BIN_COUNT*sizeof(unsigned int),cudaMemcpyDeviceToHost); CHK_ERROR

	goto Free;
Error:	
	std::cerr << "Error on CUDA: " << cudaGetErrorString(erro);

Free:
	cudaFree(d_image);
	cudaFree(d_PartialHistograms);
	cudaFree(d_Histogram);
}